#!/usr/bin/env python3
# Copyright 2012-13 Qtrac Ltd. All rights reserved.
# This program or module is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version. It is provided for
# educational purposes and is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# This program has been modified from its original source 
# (formbuilder.py) to fit in Advanced Programming 2016 week 2 exercise. 

# Week 2 exercise instructions:
#
# 1. Fill in the blank methods (except add_text()) in HtmlFormBuilder 
# class
# 2. Test the program to see whether it is able to create HTML- and
# Tk-based login form 
# 3. Implement create_register_form() function
# 4. Modify and test the program to demonstrate that the program can 
# create HTML- and Tk-based register form
# 5. Add new abstract method, add_text(), in AbstractFormBuilder classo
# correctly
# 6. Implement add_text() in HtmlFormBuilder and TkFormBuilder
# 7. Design a new form and try implement the function to build it
# 8. Modify and test the program to demonstrate that the program can 
# create the new form as HTML and Tk form
# 9. Modify and test the program to demonstrate that add_text() method 
# is working properly (e.g. create another form/modify existing one 
# where one element is created by calling add_text())

import abc
import re
import sys
if sys.version_info[:2] < (3, 2):
    from xml.sax.saxutils import escape
else:
    from html import escape

def main():
    htmlFilename = "login.html"
    htmlForm = create_login_form(HtmlFormBuilder())
    with open(htmlFilename, "w", encoding="utf-8") as file:
        file.write(htmlForm)
    print("Wrote:", htmlFilename)

    tkFilename = "login.py"
    tkForm = create_login_form(TkFormBuilder())
    with open(tkFilename, "w", encoding="utf-8") as file:
        file.write(tkForm)
    print("Wrote:", tkFilename)

    htmlFilename = "register.html"
    htmlForm = create_register_form(HtmlFormBuilder())
    with open(htmlFilename, "w", encoding="utf-8") as file:
        file.write(htmlForm)
    print("Wrote:", htmlFilename)

    tkFilename = "register.py"
    tkForm = create_register_form(TkFormBuilder())
    with open(tkFilename, "w", encoding="utf-8") as file:
        file.write(tkForm)
    print("Wrote:", tkFilename)t

    htmlFilename = "contact.html"
    htmlForm = create_contact_form(HtmlFormBuilder())
    with open(htmlFilename, "w", encoding="utf-8") as file:
        file.write(htmlForm)
    print("Wrote:", htmlFilename)

    tkFilename = "contact.py"
    tkForm = create_contact_form(TkFormBuilder())
    with open(tkFilename, "w", encoding="utf-8") as file:
        file.write(tkForm)
    print("Wrote:", tkFilename)


def create_login_form(builder):
    builder.add_title("Login")
    builder.add_label("Username", 0, 0, target="username")
    builder.add_entry("username", 0, 1)
    builder.add_label("Password", 1, 0, target="password")
    builder.add_entry("password", 1, 1, kind="password")
    builder.add_button("Login", 2, 0)
    builder.add_button("Cancel", 2, 1)
    return builder.form()

def create_register_form(builder):
    """
    Creates a registration form using given builder object.
    """
    builder.add_title("Register")
    builder.add_label("Username", 0, 0, target="username")
    builder.add_entry("username", 0, 1)
    builder.add_label("Password", 1, 0, target="password")
    builder.add_entry("password", 1, 1, kind="password")
    builder.add_label("Password verification", 2, 0, target="passwordver")
    builder.add_entry("passwordver", 2, 1, kind="password")
    builder.add_label("Email", 3, 0, target="email")
    builder.add_entry("email", 3, 1)
    builder.add_button("Register", 4, 0)
    builder.add_button("Cancel", 4, 1)
    return builder.form()


def create_contact_form(builder):
    builder.add_title("Contact")
    builder.add_text("We are reachable through this form. Please put in your contact so we can reach back to you.",
                     0, 0)
    builder.add_label("Name", 1, 0, target="name")
    builder.add_entry("name", 1, 1)
    builder.add_label("Email", 2, 0, target="email")
    builder.add_entry("email", 2, 1)
    builder.add_label("Enquiry", 3, 0)
    builder.add_entry("Enquiry", 3, 1)
    builder.add_button("Send Enquiry", 4,0)
    return builder.form()

class AbstractFormBuilder(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def add_title(self, title):
        self.title = title


    @abc.abstractmethod
    def form(self):
        pass


    @abc.abstractmethod
    def add_label(self, text, row, column, **kwargs):
        pass


    @abc.abstractmethod
    def add_entry(self, variable, row, column, **kwargs):
        pass


    @abc.abstractmethod
    def add_button(self, text, row, column, **kwargs):
        pass

    @abc.abstractmethod
    def add_text(self, text, row, column, **kwargs):
        pass

class HtmlFormBuilder(AbstractFormBuilder):

    def __init__(self):
        self.title = "HtmlFormBuilder"
        self.items = {}


    def add_title(self, title):
        self.title = title


    def add_label(self, text, row, column, **kwargs):
        labelString = "<td><p>{}</p></td>".format(text)
        self.items[row,column] = labelString

    def add_entry(self, variable, row, column, **kwargs):
        entryString = "<td><input type=\"text\" name= \"{}\"></td>".format(variable)
        self.items[row,column] = entryString

    def add_button(self, text, row, column, **kwargs):
        buttonString = "<td><input type=\"submit\" value=\"{}\"></td>".format(text)
        self.items[row,column] = buttonString

    def add_text(self, text, row, column, **kwargs):
        textString = "<td><p>{}</p></td>".format(text)
        self.items[row,column] = textString

    def form(self):
        html = ["<!doctype html>\n<html><head><title>{}</title></head>"
                "<body>".format(self.title), '<form><table border="0">']
        thisRow = None
        for key, value in sorted(self.items.items()):
            row, column = key
            if thisRow is None:
                html.append("  <tr>")
            elif thisRow != row:
                html.append("  </tr>\n  <tr>")
            thisRow = row
            html.append("    " + value)
        html.append("  </tr>\n</table></form></body></html>")
        print(html)
        return "\n".join(html)


class TkFormBuilder(AbstractFormBuilder):

    TEMPLATE = """#!/usr/bin/env python3
import tkinter as tk
import tkinter.ttk as ttk

class {name}Form(tk.Toplevel):

    def __init__(self, master):
        super().__init__(master)
        self.withdraw()     # hide until ready to show
        self.title("{title}")
        {statements}
        self.bind("<Escape>", lambda *args: self.destroy())
        self.deiconify()    # show when widgets are created and laid out
        if self.winfo_viewable():
            self.transient(master)
        self.wait_visibility()
        self.grab_set()
        self.wait_window(self)

if __name__ == "__main__":
    application = tk.Tk()
    window = {name}Form(application)
    application.protocol("WM_DELETE_WINDOW", application.quit)
    application.mainloop()
"""

    def __init__(self):
        self.title = "TkFormBuilder"
        self.statements = []


    def add_title(self, title):
        super().add_title(title)


    def add_label(self, text, row, column, **kwargs):
        name = self._canonicalize(text)
        create = """self.{}Label = ttk.Label(self, text="{}:")""".format(
                name, text)
        layout = """self.{}Label.grid(row={}, column={}, sticky=tk.W, \
padx="0.75m", pady="0.75m")""".format(name, row, column)
        self.statements.extend((create, layout))


    def add_entry(self, variable, row, column, **kwargs):
        name = self._canonicalize(variable)
        extra = "" if kwargs.get("kind") != "password" else ', show="*"'
        create = "self.{}Entry = ttk.Entry(self{})".format(name, extra)
        layout = """self.{}Entry.grid(row={}, column={}, sticky=(\
tk.W, tk.E), padx="0.75m", pady="0.75m")""".format(name, row, column)
        self.statements.extend((create, layout))


    def add_button(self, text, row, column, **kwargs):
        name = self._canonicalize(text)
        create = ("""self.{}Button = ttk.Button(self, text="{}")"""
                .format(name, text))
        layout = """self.{}Button.grid(row={}, column={}, padx="0.75m", \
pady="0.75m")""".format(name, row, column)
        self.statements.extend((create, layout))

    def add_text(self, text, row, column, **kwargs):
        name = self._canonicalize(text)
        create = ("""self.{}Text = ttk.Label(self,text="{}")""".format(name,text))
        layout = """self.{}Text.grid(row={}, column={}, padx="0.75m", \
pady="0.75m")""".format(name, row, column)
        self.statements.extend((create,layout))

    def form(self):
        return TkFormBuilder.TEMPLATE.format(title=self.title,
                name=self._canonicalize(self.title, False),
                statements="\n        ".join(self.statements))


    def _canonicalize(self, text, startLower=True):
        text = re.sub(r"\W+", "", text)
        if text[0].isdigit():
            return "_" + text
        return text if not startLower else text[0].lower() + text[1:]

if __name__ == "__main__":
    main()
